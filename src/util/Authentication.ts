import {sign, verify} from 'jsonwebtoken';
import {BungieRequest, ResponseHelper, UnauthorizedError} from './index';
import {NextFunction, Request, Response} from 'express-serve-static-core';
import {User} from '../api/models';
import moment = require('moment');
import {UserController} from '../api/controllers';

const SECRET = process.env.JWT_SECRET || 'BEST_SECRET_EU';
// Default to 12 hours
const EXPIRES_IN: number = process.env.EXPIRES_IN ? +process.env.EXPIRES_IN : 12 * 3600;

/**
 * Module that holds all functions related to authentication.
 *
 * @author Stefan Breetveld
 */
export module Authentication {

  /**
   * Verify if the given token is valid and decrypt it.
   * @param token a possibly valid jwt token.
   *
   * @throws {@link VerifyErrors} when an error occurs during verification.
   * @return The decrypted token.
   */
  export function verifyToken(token: string): Promise<TokenDetails> {
    return new Promise((resolve, reject) => {
      verify(token, SECRET, (err, decoded) => {
        if (err) {
          reject(err);
        } else {
          resolve(decoded as TokenDetails);
        }
      });
    });
  }

  /**
   * Sign TokenDetails as a valid jwt token ready to be sent to the end user.
   * @param details any details you want to send back to the user
   * @return signed jwt token
   */
  export function signToken(details: TokenDetails = new TokenDetails()): string {
    // Filter out possible password field from session data.
    delete details.sessionData.password;
    return sign({maxAge: details.maxAge, sessionData: details.sessionData}, SECRET, {
      algorithm: 'HS256',
      expiresIn: details.maxAge
    });
  }

  /**
   * Verify if a given request is valid and if sent the request further along the call chain.
   * When a request is not properly authorized the request will stop here and respond with and appropriate unauthorized
   * response code
   * @param req Any received request
   * @param res Where the possible Unauthorized response will be sent
   * @param next the next piece of middleware or the requested resource
   */
  export function verifyAuthentication(req: Request, res: Response, next: NextFunction) {
    if (res.locals.user) {
      next();
    } else {
      ResponseHelper.sendError(req, res, new UnauthorizedError('No valid token found.'));
    }
  }

  export async function userMiddleware(req: Request, res: Response, next: NextFunction) {
    const authHeader: string = req.header('authorization') || '';
    const parts = authHeader.split(' ');
    try {
      if (authHeader.toLowerCase().startsWith('bearer') && parts.length === 2) {
        const token = parts[1];
        const decodedToken = await verifyToken(token);
        const user = await User.getRepository().findOne(decodedToken.sessionData.membership_id);
        if (user) {
          res.locals.user = user;
        }
      }
    } catch (e) {
      // Can't find user if there is no token
    }
    next();
  }

  export async function refreshMiddleware(req: Request, res: Response, next: NextFunction) {
    const user: User = res.locals.user;
    if (!user) {
      next();
    }
    const now = moment();
    if (moment(user.expires).isBefore(now)) {
      if (moment(user.refresh_expires).isBefore(now)) {
        ResponseHelper.sendError(req, res, new UnauthorizedError('Token has expired'));
        return;
      }
      try {
        await UserController.refresh(req, res);
      } catch (e) {
        ResponseHelper.sendError(req, res, new UnauthorizedError('Token has expired'));
        return;
      }
    }
    next();
  }
}

export interface TokenPayload {
  token: string;
}

/**
 * Hold any data that should be included in the token as part of the payload.
 *
 * @author Stefan Breetveld
 */
export class TokenDetails {
  maxAge: number;
  sessionData: { [key: string]: any };

  /**
   * Create a new instance of this class.
   *
   * @param sessionData any data that should be included in the token.
   * @param maxAge the total time this token should be valid in milli-seconds
   */
  constructor(sessionData: { [key: string]: any } = {}, maxAge: number = EXPIRES_IN) {
    this.maxAge = maxAge;
    this.sessionData = sessionData;
  }
}

