export async function escape(text: string) {
  const tagsToReplace: {[key: string]: string} = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;'
  };
  return text.replace(/[&<>]/g, function(tag) {
    return tagsToReplace[tag] || tag;
  });
}
