import Axios, {AxiosRequestConfig, AxiosResponse, AxiosInstance} from 'axios';
import {User} from '../api/models';
import {NextFunction, Request, Response} from 'express-serve-static-core';
import moment = require('moment');
import {LOGGER} from './Logger';

export const BUNGIE_CLIENT_SECRET = process.env.BUNGIE_CLIENT_SECRET;
export const BUNGIE_API_KEY = process.env.BUNGIE_API_KEY;
export const BUNGIE_CLIENT_ID = process.env.BUNGIE_CLIENT_ID;


export class BungieRequest {
  private readonly user?: User;
  private _axios?: AxiosInstance;
  private get axios(): AxiosInstance {
    return this._axios || this.create();
  }

  constructor(user?: User) {
    this.user = user;
  }

  private create() {
    this._axios = Axios.create({
      baseURL: 'https://www.bungie.net/Platform',
      headers: {
        'User-Agent': `Pursuit Tracker/${process.env.npm_package_version || 'beta'} AppId/${BUNGIE_CLIENT_ID} (+;spiderbiggen@gmail.com)`,
        'X-API-Key': BUNGIE_API_KEY
      }
    });
    this.axios.interceptors.response.use(
      (response) => {
        if (response.data.Response) {
          response.data = response.data.Response;
        }
        return response;
      },
      error => {throw error;}
    );
    if (this.user && moment(this.user.expires).isAfter()) {
      this.axios.defaults.headers['Authorization'] = `bearer ${this.user.access_token}`;
    }
    return this.axios;
  }

  async get<T = any>(url: string, config?: AxiosRequestConfig): Promise<T> {
    return (await this.axios.get<T>(url, config)).data;
  }

  async post<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
    return (await this.axios.post<T>(url, data, config)).data;
  }

  static middleWare(req: Request, res: Response, next: NextFunction) {
    res.locals.api = new BungieRequest(res.locals.user);
    next();
  }
}


