import {
  Column,
  Entity,
  EntityRepository,
  getCustomRepository,
  getRepository,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  Repository
} from 'typeorm';
import moment = require('moment');
import {BungieTokenResponse} from '../types';

/**
 * Entity that holds information about a user.
 *
 * @author Stefan Breetveld
 */
@Entity('USERS')
export class User {

  @PrimaryColumn()
  membership_id: string;
  @Column()
  access_token: string;
  @Column()
  token_type: string;
  @Column({type: 'datetime'})
  expires: Date;
  @Column()
  refresh_token: string;
  @Column({type: 'datetime'})
  refresh_expires: Date;

  static getRepository() {
    return getCustomRepository(UserRepository);
  }

}

@EntityRepository(User)
export class UserRepository extends Repository<User> {

  async createFromTokenData(data: BungieTokenResponse) {
    const userObj: Partial<User> = Object.assign({}, data);
    userObj.refresh_expires = moment().add(data.refresh_expires_in, 's').toDate();
    userObj.expires = moment().add(data.expires_in, 's').toDate();
    return await this.save(this.create(userObj));
  }

  async updateFromTokenData(data: BungieTokenResponse) {
    const userObj: Partial<User> = Object.assign({}, data);
    userObj.refresh_expires = moment().add(data.refresh_expires_in, 's').toDate();
    userObj.expires = moment().add(data.expires_in, 's').toDate();
    // @ts-ignore
    delete userObj.expires_in;
    // @ts-ignore
    delete userObj.refresh_expires_in;
    return await this.update(data.membership_id, userObj);
  }
}
