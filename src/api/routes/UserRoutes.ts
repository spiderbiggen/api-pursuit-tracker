import {UserController} from '../controllers';
import {RouteWrapper} from '../../routing';
import {Authentication} from '../../util';

const router = new RouteWrapper('/users');

router.post('/authenticate', UserController.loginUser);

const authenticatedRouter = new RouteWrapper('');

authenticatedRouter.registerMiddleware(Authentication.verifyAuthentication);
authenticatedRouter.get('/me', UserController.self);

router.subRoutes(authenticatedRouter);

export default router;
