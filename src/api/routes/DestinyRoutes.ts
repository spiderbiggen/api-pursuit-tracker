import {RouteWrapper} from '../../routing';
import {Authentication} from '../../util';
import {DestinyController} from '../controllers';

const router = new RouteWrapper('/destiny2');

router.registerMiddleware(Authentication.verifyAuthentication);
router.registerMiddleware(Authentication.refreshMiddleware);
router.get('/:type/profile/:id', DestinyController.profile);
router.get('/manifest', DestinyController.manifest);

export default router;
