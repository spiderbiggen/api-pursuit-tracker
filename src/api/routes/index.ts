import UserRoutes from './UserRoutes';
import DestinyRoutes from './DestinyRoutes';
/**
 * Export a list of all routes
 */
export default [UserRoutes, DestinyRoutes];
