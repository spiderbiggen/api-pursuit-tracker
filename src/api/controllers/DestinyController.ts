import {Request, Response} from 'express';
import {BungieRequest, LOGGER} from '../../util';
import {DestinyManifest, DestinyProfileResponse} from 'bungie-api-ts/destiny2';
import {fromBuffer, ZipFile} from 'yauzl';
import * as fs from 'fs';
import {Readable} from 'stream';

/**
 * Module that has all resources for managing users.
 *
 * @author Stefan Breetveld
 */
export module DestinyController {

  export async function profile(req: Request, res: Response): Promise<any> {
    const {type, id} = req.params;
    const api: BungieRequest = res.locals.api;
    return await api.get<DestinyProfileResponse>(`/Destiny2/${type}/Profile/${id}/`);
  }

  function promisify<T = any>(api: any, ...args: any[]): Promise<T> {
    return new Promise(function (resolve, reject) {
      api(...args, (err: any, response: T) => {
        if (err) {
          return reject(err);
        }
        resolve(response);
      });
    });
  }


  export async function manifest(req: Request, res: Response) {
    const api: BungieRequest = res.locals.api;
    const response = await api.get<DestinyManifest>('/Destiny2/Manifest/');
    const data = await api.get(`https://www.bungie.net${response.mobileWorldContentPaths['en']}`, {responseType: 'arraybuffer'});
    const zipFile: ZipFile = await promisify<ZipFile>(fromBuffer, data);
    const writeStream = fs.createWriteStream('d2_data.db');
    await new Promise((resolve, reject) => {
      zipFile.readEntry();
      zipFile.on('entry', async (entry) => {
        const stream = await promisify<Readable>(zipFile.openReadStream.bind(zipFile), entry);
        stream.on('end', () => {
          zipFile.readEntry();
        });
        stream.pipe(writeStream);
      });
      zipFile.on('end', () => {
        writeStream.close();
      });
      resolve();
    });
    return null;
  }

}
