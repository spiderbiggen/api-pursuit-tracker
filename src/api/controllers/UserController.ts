import {Request, Response} from 'express';
import {
  Authentication,
  BUNGIE_CLIENT_ID,
  BUNGIE_CLIENT_SECRET,
  BungieRequest,
  LOGGER,
  TokenDetails,
  TokenPayload,
  UnauthorizedError
} from '../../util';
import {User} from '../models';
import {stringify} from 'querystring';
import {BungieTokenResponse} from '../types';
import {UserMembershipData} from 'bungie-api-ts/user';

/**
 * Module that has all resources for managing users.
 *
 * @author Stefan Breetveld
 */
export module UserController {

  export async function loginUser(req: Request, res: Response): Promise<TokenPayload> {
    const code = req.body.code;
    try {
      const formData = stringify({
        grant_type: 'authorization_code',
        code: code,
        client_secret: BUNGIE_CLIENT_SECRET,
        client_id: BUNGIE_CLIENT_ID,
      });
      const api: BungieRequest = res.locals.api;
      const response = await api.post<BungieTokenResponse>('App/OAuth/token/', formData,
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          }
        }
      );
      const user = await User.getRepository().createFromTokenData(response);
      LOGGER.debug(`Successfully logged in User(${response.membership_id})`);
      return {token: Authentication.signToken(new TokenDetails({membership_id: user.membership_id}))};
    } catch (e) {
      LOGGER.error(e);
      throw new UnauthorizedError(undefined, e);
    }
  }

  export async function refresh(req: Request, res: Response): Promise<TokenPayload> {
    const user: User = res.locals.user;
    try {
      let data = stringify({
        grant_type: 'refresh_token',
        refresh_token: user.refresh_token,
        client_secret: BUNGIE_CLIENT_SECRET,
        client_id: BUNGIE_CLIENT_ID,
      });
      LOGGER.debug(`DATA: ${data}`);
      const response = await new BungieRequest(user).post<BungieTokenResponse>('App/OAuth/token/', data,
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          }
        }
      );
      const newUser = await User.getRepository().updateFromTokenData(response);
      LOGGER.debug(`Successfully refreshed token for User(${response.membership_id})`);
      return {token: Authentication.signToken(new TokenDetails(newUser))};
    } catch (e) {
      LOGGER.error(e);
      throw new UnauthorizedError(undefined, e);
    }
  }

  export async function self(req: Request, res: Response): Promise<UserMembershipData> {
    const api: BungieRequest = res.locals.api;
    return await api.get<UserMembershipData>('/User/GetMembershipsForCurrentUser/');
  }

}
