export type BungieTokenResponse = {
  access_token: string;
  token_type: string;
  expires_in: number;
  refresh_token: string;
  refresh_expires_in: number;
  membership_id: string;
}

export type DestinyMemberShip = {
  LastSeenDisplayName: string
  LastSeenDisplayNameType: number
  applicableMembershipTypes: number[]
  crossSaveOverride: number
  displayName: string
  iconPath: string
  isPublic: boolean
  membershipId: string
  membershipType: number
}

export type BungieUser = {
  about: string
  blizzardDisplayName: string
  context: {
    isFollowing: boolean
    ignoreStatus: {
      ignoreFlags: number
      isIgnored: boolean
    }
  }
  displayName: string
  firstAccess: Date
  isDeleted: boolean
  lastUpdate: Date
  locale: string
  localeInheritDefault: boolean
  membershipId: string
  profilePicture: number
  profilePicturePath: string
  profileTheme: number
  profileThemeName: string
  showActivity: boolean
  showGroupMessaging: boolean
  statusDate: Date
  statusText: string
  steamDisplayName: string
  successMessageFlags: string
  uniqueName: string
  userTitle: number
  userTitleDisplay: string
  xboxDisplayName: string
}

export type BungieProfileResponse = {
  bungieNetUser: BungieUser;
  destinyMemberships: DestinyMemberShip[]
}
