module.exports = {
  type: 'sqlite',
  database: './pursuit_tracker.db',
  entities: [
    './build/api/models/**/*.js'
  ],
  timezone: 'Z',
  synchronize: true,
  logging: false,
  cache: true
};
